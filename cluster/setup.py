#USAGE - python setup.py path/to/whitelist/in/S3
#Downloads the list of WET archive URLs from CommonCrawl,
#as well as the desired whitelist


import boto3
import os, sys

COMMONCRAWL_SEED_URL = 'crawl-data/CC-MAIN-2016-22/wet.paths.gz'

os.system('rm urls')
os.system('rm whitelist.txt')

s3 = boto3.client('s3')
s3.download_file("commoncrawl", COMMONCRAWL_SEED_URL, 'urls.gz')
os.system('gzip -d urls.gz')

s3.download_file('darkmatter-training', sys.argv[1], 'whitelist.txt')

