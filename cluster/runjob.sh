#USAGE: ./runjob.sh [topicName] [NODE_NUMBER] [THRESHOLD]

#This shell script starts three threads all processing
#Different archives in commoncrawl. It is the recommended
#way to start a crawl as part of a larger cluster. The first
#argument to the script is the nodeID, and the second is the 
#number of nodes. The last argument is the threshold for
#page acceptance. Consult cluster.py for more details

python setup.py $1/whitelist-final.txt

if [ ! -f whitelist.txt ]; then
    	echo "ERROR: WHITELIST NOT FOUND"
	echo "Stopping now..."
	exit
fi

n1=$(($2 * 3 - 3))
n2=$(($2 * 3 - 2))
n3=$(($2 * 3 - 1))


python cluster.py $n1 30 $3 &
python cluster.py $n2 30 $3 &
python cluster.py $n3 30 $3 &
wait

#gather all results into one big file
cat `ls | grep output` > finalList.txt
rm `ls | grep output`

#upload results to s3
aws s3 cp finalList.txt s3://darkmatter-training/$1/results/$1$2

#text jared
python notify.py $1 $2
