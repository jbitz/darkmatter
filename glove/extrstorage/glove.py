######################################################
# glove.py downloads WET files from commoncrawl and  #
# turns them into one file of space separated tokens #
# so that GloVe can train word vectors on the corpus #
#                                                    #
# usage: python glove.py [num]                       #
# num: the number of archives to download (if none   #
#      is specified, use all of commoncrawl)         #
######################################################

import boto3
import nltk
import os, sys, re

COMMONCRAWL_SEED_URL = 'crawl-data/CC-MAIN-2016-22/wet.paths.gz'
CLEANING_REGEX = re.compile(r'[^a-zA-Z0-9-]')

def getURLs(s3, nodeID, numNodes):
    s3.download_file("commoncrawl", COMMONCRAWL_SEED_URL, 'urls.gz')
    os.system('gzip -d urls.gz')
    counter = 0
    chosenURLs = []

    with open('urls') as urls:
        for line in urls:
            chosenURLs.append(line[:len(line) - 1])
            counter += 1

    print(chosenURLs[1])
    os.system('rm urls')
    return chosenURLs

def cleanPage(page):
    page = page[page.find('Content-Length') + 20:]
    page = CLEANING_REGEX.sub(' ', page)
    tokens = nltk.WordPunctTokenizer().tokenize(page)
    clean = [token.lower() for token in tokens]
    s = ""
    for c in clean:
        s += c + " "
    s += " "
    return s
   
def processWETArchive(s3, url, worker_id, counter):
    filename = str(worker_id)
    s3.download_file("commoncrawl", url, filename + '.gz')
    os.system('gzip -d ' + filename + '.gz')
    total = ' '

    with open(filename) as wetFile:
        pages = wetFile.read().split('WARC/1.0')
        count = 1
        for page in pages:
            if count % 1000 == 0:
                print(count)
            count += 1

            result = cleanPage(page)
            total += result

    with open('output-node' + filename + '-' + str(counter) + '.txt', 'w') as outFile:
        outFile.write(total)

    os.system('rm ' + filename)


def main():
    maxArchives = -1
    if len(sys.argv) > 1:
        maxArchives = int(sys.argv[1])
    s3 = boto3.client('s3')
    urls = getURLs(s3, 0, 0)

    for counter, url in enumerate(urls):
        if maxArchives != -1 and counter >= maxArchives:
            break
        print("PROCESSING ARCHIVE #" + str(counter))
        processWETArchive(s3, url, 0, counter)
    
    os.sys('cat `ls | grep output` | tr "\n" " " > final.txt')
    os.sys('rm `ls | grep output`')
    return 0

if __name__ == '__main__':
    main()
