# USAGE:
# python prepareForMT.py [infile] [outfile]
# The outfile is correctly formatted to upload
# directly to MTurk

import sys

lines = []
with open(sys.argv[1]) as f:
    for line in f:
        word = line[6:line.find(',')]#chop of the c/en/ at the start and the parent term at the end
        if not word in lines:
            lines.append(word)

with open(sys.argv[2], 'w') as f:
    f.write('url,word\n')
    for word in lines:
        url_formatted_word = word.replace("_", "%20")
        url = "http://www.google.com/#q=" + url_formatted_word
        f.write(url + ',' + word + '\n')
