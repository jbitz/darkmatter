#!/usr/bin/python
#USAGE: ./createWhitelist.py [TOPIC]

import sys, os
from math import log

with open('./' + sys.argv[1] + '/whitelist.csv') as inFile:
    data = [ (line.split(',')[0].replace('_', ' '), log(float(line.split(',')[1]), 10)) for line in inFile ] #Damn son
    with open('./' + sys.argv[1] + '/whitelist-initial.txt', 'w') as outFile:
        for term in data:
            outFile.write(term[0] + ',' + str(term[1]) + '\n')

print('Uploading to S3...')
os.system('aws s3 cp ./' + sys.argv[1] + '/whitelist-initial.txt s3://darkmatter-training/' + sys.argv[1] + '/whitelist-initial.txt')
