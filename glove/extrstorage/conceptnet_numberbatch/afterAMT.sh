#!/bin/bash

currpath=$1/r$2 #first input is the topic name, second input is the round number
toppath=$1
nxt=$[$2+1]
nxtpath=$1/r$nxt

python removeIrrelevant.py $currpath/fromAMT.csv $currpath/relevant.csv $currpath/irrelevant.csv #remove irrelevants

cat $currpath/relevant.csv >> $toppath/whitelist.csv #update whitelist

cat $currpath/irrelevant.csv >> $toppath/irrelevant.csv #update irrelevants

python getMaximal.py $currpath/relevant.csv > $currpath/maximal.csv #get maximal for new seeds

mkdir $nxtpath

python randomSample.py $currpath/maximal.csv $nxtpath/seeds.csv




