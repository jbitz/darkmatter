# NUMBERBATCH.PY
# Usage: python numberbatch.py [input_file] > [output_file]
# Or:    python numberbatch.py (and then use the interactive REPL)
# 
# Input: a keyword, or file with \n-separated keywords
# Output: runs Conceptnet-Numberbatch on them and prints top 100 keywords

import sys
import numpy
from conceptnet5.nodes import standardized_concept_uri

N = 100 #Results to display

def getIndex(word, vocab):
    for idx, w in enumerate(vocab):
        if w == word: return idx
    return -1

def printResults(distances, vocab, parentword):
    sorted = numpy.argsort(-distances)

    # print("\nResults:")
        
    count = 0
    i = 0
    while count < N:
        if vocab[sorted[i]].find('c/en') > -1: #We only want english words
            print( (vocab[sorted[i]]) + "," + parentword )
            count += 1
        i += 1

def loadVocab():
    vocab = []
    with open('conceptnet-ensemble-201603-labels.txt') as namesFile:
        for idx, line in enumerate(namesFile):
            l = ''.join(chr(ord(x)) for x in line) #I hate unicode
            l = l[:len(l) - 1] #chop off \n
            vocab.append(l)
    return vocab

def main():
    if len(sys.argv) < 2: # manual use
        ### Load the Numberbatch ###
        print('Loading Conceptnet Numberbatch...')
        W = numpy.load('conceptnet-ensemble-201603-600d.npy')
        print('Finished! Loaded ' + str(len(W)) + ' vectors!')
        print('Loading vocab...')
        vocab = loadVocab()
        print('Finished loading ' + str(len(vocab)) + ' words in vocab!')

        vector = numpy.zeros((1, 600))

        while(True):
            ### Get user input and normalize it into the ConceptNet standard form ###
            word = raw_input('What word would you like to search for? (type "a" for analogies, and "vs" for vector sum query) ')
            if word == '':
                break
            elif word == 'a': ##Try analogies
                print("A is to B as C is to _______")
                a = raw_input("A? ")
                b = raw_input("B? ")
                c = raw_input("C? ")
                a = standardized_concept_uri('en', a)
                b = standardized_concept_uri('en', b)
                c = standardized_concept_uri('en', c)
                a = ''.join(chr(ord(x)) for x in a) #I hate unicode
                b = ''.join(chr(ord(x)) for x in b) #I hate unicode
                c = ''.join(chr(ord(x)) for x in c) #I hate unicode
                aVec = W[getIndex(a, vocab)]
                bVec = W[getIndex(b, vocab)]
                cVec = W[getIndex(c, vocab)]
                vector = bVec - aVec + cVec
                vector = vector / numpy.linalg.norm(vector)
            elif word == 'vs':
                phrase = raw_input('Multi-word query? ')
                terms = phrase.split(' ')
                for i in range(len(terms)):
#                for term in phrase.split(' '):
                    term = terms[i]
                    standard = standardized_concept_uri('en', term)
                    standard = ''.join(chr(ord(x)) for x in standard)
                    print('Standardized form: ' + standard)
                    index = getIndex(standard, vocab)
                    print('Index: ' + str(index))
                    if (i == 0):
                        vector = W[index]
                    else:
                        vector += W[index]
            else:
                word = standardized_concept_uri('en', word)
                word = ''.join(chr(ord(x)) for x in word) #I hate unicode
                print('Standardized form: ' + word)

                ### Find the corresponding vector ###
                index = getIndex(word, vocab)
                if index == -1:
                    print('That word is not in the dictionary! Please try again')
                    continue

                vector = W[index]

            vector /= numpy.linalg.norm(vector)

            distances = numpy.dot(W, vector.T)
            printResults(distances, vocab, word)
    
    else: #running from script
        with open(sys.argv[1], 'r') as f:
            W = numpy.load('conceptnet-ensemble-201603-600d.npy')
            for line in f:
                line = line.split(',')
                vocab = loadVocab()
                vector = numpy.zeros((1, 600))

                word = line[0]
                if ' ' in word: #multi-word query from original seed (two words separated by space)
                    terms = word.split(' ')
                    for i in range(len(terms)):
                        term = terms[i]
                        standard = standardized_concept_uri('en', term)
                        standard = ''.join(chr(ord(x)) for x in standard)
                        index = getIndex(standard, vocab)
                        if (i == 0):
                            vector = W[index]
                        else:
                            vector += W[index]
                else: #single-word query 
                    word = standardized_concept_uri('en', word)
                    word = ''.join(chr(ord(x)) for x in word) #I hate unicode
                    #print('Standardized form: ' + word)

                    ### Find the corresponding vector ###
                    index = getIndex(word, vocab)
                    if index == -1:
                    # if word doesn't exist in the dictionary
                        print "Error! Word " + word + " not in dictionary!"
                        return
                    vector = W[index]

                # for all queries...
                vector /= numpy.linalg.norm(vector)

                distances = numpy.dot(W, vector.T)
                printResults(distances, vocab, word)
                

main()
