#########################################################
# cluster.py                                            #
# Jared Bitz, July 2016                                 #
#                                                       #
# This script is an (admittedly janky) node in a        #
# custom cluster to process CommonCrawl                 #
# archives. Generally, you should invoke it with the    #
# runjob.sh shell script in the same directory, which   #
# will launch multiple processes operating on different #
# subsets of CommonCrawl in parallel. Before doing that #
# you should also run setup.py to download the list     #
# of WET URLs and the desired whitelist                 #
#                                                       #
# usage: python cluster.py [nodeID] [numNodes]          #
#                                                       #
# nodeID: The index of the node represented by this     #
#           specific instance of the script             #
#                                                       #
# numNodes: The total number of nodes in the cluster.   #
#           Determines how many archives to process.    #
#                                                       #
# thresh: The threshold to use on the page scoring      #
#           function.                                   #
#                                                       #
# You should also pre-download the url list and the     #
# whitelist by running setup.py before running this.    #
#                                                       #
# As a side note, if you want to test on a smaller      #
# portion of CommonCrawl, just specify a very high      #
# value for numNodes. E.g. 'cluster.py 0 12000 7.0'     #
# will make the thread for this script evaluate only    #
# three WET archives, since commoncrawl contains a      #
# little over 24,000.                                   #
#########################################################

import boto3 #For getting S3 data from Commoncrawl
import nltk #For tokenizer
import os, sys, re

BAD_URL = 'does not pass'
CLEANING_REGEX = re.compile(r'[^a-zA-Z0-9]')
URL_OFFSET = len('WARC-Target-URI: ')
DOCUMENT_COUNTER = "DOCUMENT_COUNT" #Hopefully never a keyword

#Load the pre-downloaded list of urls
def getURLs(s3, nodeID, numNodes):
    counter = 0
    chosenURLs = []
    with open('urls') as urls:
        for line in urls:
            #Take only the archive urls this node should process
            if counter % numNodes == nodeID:
                chosenURLs.append(line[:len(line) - 1])
            counter += 1

    return chosenURLs

#Load the pre-downloaded whitelist, and return
#as a dictionary of term->weight
def getWords():
    weights = {}
    with open('whitelist.txt') as f:
        for line in f:
            w = line[:line.find(',')]
            weights[w] = 0

    weights[DOCUMENT_COUNTER] = 0

    return weights

def filterPage(page, words):
    if not ' the ' in page: #check for english
        return

    words[DOCUMENT_COUNTER] += 1
    
    page = CLEANING_REGEX.sub(' ', page) #Get rid of non alpha-numerics
    tokens = nltk.WordPunctTokenizer().tokenize(page)
    clean = [token.lower() for token in tokens] #All comparison in lower case
    freq = nltk.probability.FreqDist(clean)

    for word in words:
        if (word.find(' ') > -1 and word in page) or (word in freq):
            words[word] += 1

#Downloads the WET archive from a given url and processes
#Each of it's pages in turn. It's important to note that
#This process saves the accepted URLs from each archive separately.
#These all get combined with some bash scripting in runjob.sh after
#this script completes.
def processWETArchive(s3, url, words, worker_id, counter):
    filename = str(worker_id)

    #Download and unzip the archive.
    s3.download_file("commoncrawl", url, filename + '.gz')
    os.system('gzip -d ' + filename + '.gz')

    with open(filename) as wetFile:
        pages = wetFile.read().split('WARC/1.0')
        count = 0
        for page in pages:
            if count % 1000 == 0:
                print(count)
            count += 1

            filterPage(page, words)

    #Use our worker's id as a filename to avoid conflicts with other threads
    with open('output-node' + filename + '-' + str(counter) + '.txt', 'w') as outFile:
        for s in words:
            outFile.write(s + ',' + str(words[s]) + '\n')
 
    os.system('rm ' + filename) #clean up the archive we downloaded

def main():
    s3 = boto3.client('s3')

    urls = getURLs(s3, int(sys.argv[1]), int(sys.argv[2]))
    words = getWords()

    for counter, url in enumerate(urls):
        print("PROCESSING ARCHIVE # " + str(counter))
        processWETArchive(s3, url, words, int(sys.argv[1]), counter)

    with open('final-counts-' + sys.argv[1] + '.txt', 'w') as outFile:
        for s in words:
            outFile.write(s + ',' + str(words[s]) + '\n')

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Invalid arguments. You probably made a mistake in preparing runjob.sh")
        print("Consult the top comment in the source code for more information")
        sys.exit(1)
    else:
        main()
        sys.exit(0)
