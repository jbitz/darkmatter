#This shell script starts three threads all processing
#Different archives in commoncrawl. It is the recommended
#way to start a crawl as part of a larger cluster. The first
#argument to the script is the nodeID, and the second is the 
#number of nodes. The last argument is the threshold for
#page acceptance. Consult cluster.py for more details

echo Start Time: `date` > timing.txt
python setup.py $1

python idf.py 0 200 & 
python idf.py 1 200 & 
python idf.py 2 200 &
wait

rm output-node*
python aggregate.py

echo Finish Time: `date` >> timing.txt
python notify.py
