#!/usr/bin/python
#USAGE - python setup.py [TOPIC]
#
#Downloads the list of WET archive URLs from CommonCrawl,
#as well as the desired whitelist. Should be used before starting
#runjob.sh


import boto3
import os, sys

COMMONCRAWL_SEED_URL = 'crawl-data/CC-MAIN-2016-22/wet.paths.gz'

os.system('rm urls')
os.system('rm whitelist.txt')
os.system('rm startTime.txt')
os.system('rm finishTime.txt')

s3 = boto3.client('s3')
s3.download_file("commoncrawl", COMMONCRAWL_SEED_URL, 'urls.gz')
os.system('gzip -d urls.gz')

s3.download_file('darkmatter-training', sys.argv[1] + '/whitelist-initial.txt', 'whitelist.txt')
