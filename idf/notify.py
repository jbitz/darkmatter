#USAGE: python notify.py

#Called by runjob.sh to alert when the current IDF job has finished
#You can change where the notification gets sent by changing the .credentials
#File in this directory. Obviously, make sure it stays in the .gitignore file, so
#the username and password don't end up online

import sys
import smtplib
import random
uid = ''
pwd = ''
to = ''

#For variety
exclamations = ['Nifty!', 'Wowzers!', 'Hooray!', 'Neat-o!', 'Well, would you look at that!']

with open('.credentials') as f:
    uid = next(f)[:-1]
    pwd = next(f)[:-1]
    to = next(f)[:-1]

exclam = exclamations[random.randint(0, len(exclamations) - 1)]
msg = exclam + ' The currently running IDF job just finished! You should go check it!'
server = smtplib.SMTP('smtp.gmail.com:587')
server.starttls()
server.login(uid, pwd)
server.sendmail('DM', to, msg)
server.quit()
