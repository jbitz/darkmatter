import math, os
counts = {}

for i in range(0, 3):
    with open('final-counts-' + str(i) + '.txt') as f:
        for line in f:
            vals = line[:-1].split(',')
            if vals[0] in counts:
                counts[vals[0]] += int(vals[1]) 
            else:
                counts[vals[0]] = int(vals[1])
                

with open('IDF.txt', 'w') as f:
    docCount = counts['DOCUMENT_COUNT']
    for word in counts:
        print(word + str(counts[word]))
        idf = 0
        if counts[word] > 0:
            idf = math.log( (docCount * 1.0) / counts[word] )
        f.write(word + ',' + str(idf) + '\n')

os.system('sort IDF.txt > IDF_SORTED.txt')
os.system('rm IDF.txt final-counts*')
