#################################################################
# crossvalidation.py                                            #
# Jared Bitz, July 2016                                         #
#                                                               #
# This script will run recall tests on a given cross-validation #
# set, given a specific list of terms and their weights and     #
# using the page scoring function descibed in cluster.py, found #
# under ~/cluster/cluster.py. It requires a whitelist of comma- #
# separated term-weight pairs to be already downloaded and      #
# saved as whitelist.txt. After testing recall on the CV set,   #
# it will prompt for a threshold to use on the test set, as a   #
# rigorous valuation of the recall for that threshold. Results  #
# are saved in a file called recall.txt afterwards - it may     #
# crash if such a file is already present in the directory.     #
#                                                               #
# Two ways to invoke:                                           #
# 1) python crossvalidation.py [numCV] [numTS]                  #
#   a) Assumes that the cross-validation and test sets are      #
#       already downloaded and located under ./cv and ./ts      #
#       respectively                                            #
#   b) numCV: The highest number of a file in the CV set        #
#   c) numTS: Same as above, but for the test set               #
#   d) Ensure that the two sets are composed of a single .txt   #
#       file for each page, and that these pages are numbered   #
#       starting at 0.txt, up to and including numCV.txt        #
#                                                               #
# 2) python crossvalidation.py [cvURL] [numCV] [tsURL] [numTS]  #
#   a) Downloads the CV set and the test set from the given     #
#       folders in the darkmatter-training S3 bucket            #
#   b) cvURL: URL for the CV set (proper format is something    #
#       like 'marijuana/crossvalidation-set', NOT               #
#       's3://darkmatter-training/marijuana/crossvalidation-set'#
#       Also, don't put a slash after the last folder           #
#   c) tsURL: Same as above, but for the test set               #
#   d) numCV and numTS: Same as in part 1                       #
#################################################################

import boto3
import nltk
import os, sys, re
import logging

BAD_URL = 'does not pass'
CLEANING_REGEX = re.compile(r'[^a-zA-Z0-9]')
#You can change this to determine which threshold values get tested on the CV set
THRESHOLDS_TO_TEST = [4.0, 4.33, 4.66, 5.0, 5.33, 5.66, 6.0, 6.33, 6.66, 7.0, 7.33, 7.66, 8.0, 8.33, 8.66, 9.0]

#Loads term weights from an already-present file
def getWeights():
    weights = {} 
    if not os.path.isfile('whitelist.txt'):
        print('Please place whitelist.txt in this directory before running.')
        sys.exit()
    with open('whitelist.txt') as f:
        for line in f:
            key = line[:line.find(',')]
            value = float(line[line.find(',') + 1: len(line) - 1])
            weights[key] = value

    return weights

#Page scoring algorithm. Read the description under ~/cluster/cluster.py
#for more information
def filterPage(page, whitelist, threshold):
    if not ' the ' in page: #check for english
        return False
    
    page = CLEANING_REGEX.sub(' ', page)
    tokens = nltk.WordPunctTokenizer().tokenize(page)
    clean = [token.lower() for token in tokens]
    freq = nltk.probability.FreqDist(clean)

    totalScore = 0.0
    for term, weight in whitelist.items():
        TF = 0
        if term.find(' ') > -1: #Multi-word term
            TF = clean.count(term)
        else: #Single word term
            TF = freq[term]
        
        weightedTF = (TF * 1.0) / len(clean)
        totalScore += weightedTF * weight * 1000 #scale for pretty numbers

    if totalScore >= threshold:
        return True

    return False

#Download the CV set or the test set
def downloadSet(s3, url, highest, folder):
    print("Downloading from " + url)
    os.system('mkdir ' + folder)
    for i in range(0, highest + 1):
        target = url + '/' + str(i) + '.txt'
        print(target)
        s3.download_file("darkmatter-training", target, folder + '/' + str(i) + '.txt')

def main(numCross, numTest, urlCross, urlTest):
    logging.getLogger('boto').setLevel(logging.CRITICAL)
    #Download cross validation set if we need to
    s3 = boto3.client('s3')
    if not os.path.isdir('./cv'):
        print('Downloading cross-validation set...')
        downloadSet(s3, urlCross, numCross, 'cv')
    
    weights = getWeights()

    #Test all the different thresholds
    print("\nDownloads Finished. Testing CV set...\n")
    print('Threshold\tAccepted\tRecall')
    #Also save everything to recall.txt
    outFile = open('recall.txt', 'w')
    outFile.write('Threshold\tAccepted\tRecall\n')
    for thresh in THRESHOLDS_TO_TEST:
        totalAccepted = 0.0
        #Filter each page
        for i in range(0, numCross + 1):
            with open('cv/' + str(i) + '.txt') as file:
                if filterPage(file.read(), weights, thresh):
                    totalAccepted += 1.0 #Make it a float for division
        
        print(str(thresh) + '\t\t' + str(totalAccepted) + '/' + str(numCross+1) + '\t' + str(totalAccepted / (numCross + 1)))
        outFile.write(str(thresh) + '\t\t' + str(totalAccepted) + '/' + str(numCross+1) + '\t' + str(totalAccepted / (numCross + 1)) + '\n')

    
    #Final recall determination on test set 
    print('')
    selected = float(raw_input("Choose your desired threshold for a recall test on the test set: "))
    if not os.path.isdir('./ts'): #Download the test set if we need to
        print('Downloading test set...')
        downloadSet(s3, urlTest, numTest, 'ts')

    totalAccepted = 0.0
    for i in range(0, numTest + 1):
            with open('ts/' + str(i) + '.txt') as file:
                if filterPage(file.read(), weights, selected):
                    totalAccepted += 1.0
    
    print("Final Recall Result:")
    print(str(selected) + '\t\t' + str(totalAccepted) + '/' + str(numTest+1) + '\t' + str(totalAccepted / (numTest + 1)))

    outFile.write('\nRecall on Test Set\n') 
    outFile.write(str(selected) + '\t\t' + str(totalAccepted) + '/' + str(numTest+1) + '\t' + str(totalAccepted / (numTest + 1)))
    outFile.close()
    
    #Uncomment these if you want to clean up the downloads after finishing
    #os.system('rm -r cv')
    #os.system('rm -r ts')

if __name__ == '__main__':
    if len(sys.argv) == 3:
        #Ignore URLs if we just specified the numbers
        main(int(sys.argv[1]), int(sys.argv[2]), '', '')
    elif len(sys.argv) == 5:
        main(int(sys.argv[2]), int(sys.argv[4]), sys.argv[1], sys.argv[3])
        sys.exit(1)
    else:
        sys.exit(0)
