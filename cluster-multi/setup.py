#Downloads the list of WET archive URLs from CommonCrawl,
#as well as the desired whitelist. Should be used before starting
#runjob.sh

#USAGE - python setup.py path/to/whitelist/in/S3

import boto3
import os, sys

COMMONCRAWL_SEED_URL = 'crawl-data/CC-MAIN-2016-22/wet.paths.gz'

os.system('rm urls')

s3 = boto3.client('s3')
s3.download_file("commoncrawl", COMMONCRAWL_SEED_URL, 'urls.gz')
os.system('gzip -d urls.gz')

for topic in sys.argv[1].split(' '):
    print(topic)
    s3.download_file('darkmatter-training', topic + '/whitelist-final.txt', 'whitelist-' + topic + '.txt')

