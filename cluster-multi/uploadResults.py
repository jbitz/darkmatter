#USAGE python uploadResults.py "[TOPICS]" [NODE_ID]

#Aggregates all of the component files into one big file for each topic,
#uploads those to the correct directories in S3,
#and then cleans up all of the extra junk left over

import os, sys

for topic in sys.argv[1].split(' '):
    os.system('cat `ls | grep ' + topic + '-output` > finalList-' + topic + '.txt')
    os.system('aws s3 cp finalList-' + topic + '.txt s3://darkmatter-training/' + topic + '/results/' + topic + sys.argv[2])
    os.system('rm `ls | grep ' + topic + '-output`')
    os.system('echo "(`cat finalList-' + topic + '.txt | wc -l` total URLs)"')
