#!/usr/bin/env python

#This server listens on port 4242 and reads a string of parameters that are used
#to start the runjob.sh script, because I'm far too lazy to ssh into each machine

import socket 
import os

host = '' 
port = 4242
backlog = 5 
size = 2048
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.bind((host,port)) 
s.listen(backlog) 
while 1: 
    client, address = s.accept() 
    data = client.recv(size)
    print('Recieved parameters: ' + data.decode())
    if data.decode() == 'update':
        os.system('mv ~/cluster-multi/cluster.py ~/cluster-multi/cluster-old.py')
        os.system('mv ~/cluster-multi/runjob.sh ~/cluster-multi/runjob-old.sh')
        os.system('aws s3 cp s3://darkmatter-training/scripts/cluster.py ~/cluster-multi/cluster.py')
        os.system('aws s3 cp s3://darkmatter-training/scripts/runjob.sh ~/cluster-multi/runjob.sh')
        os.system('chmod +x ~/cluster-multi/runjob.sh')
        print('updating...')
        client.send(bytes('update recieved'))
    else:
        #echo it back, so that the client knows what they've done
        actionStr = '~/cluster-multi/runjob.sh ' + data.decode() + ' &'
        os.system(actionStr)
        print(actionStr)
        client.send(bytes(actionStr))
    
    client.close()
