#########################################################
# cluster.py                                            
# Jared Bitz, July 2016                                
#                                                       
# This script is an (admittedly janky) node in a        
# custom cluster to process CommonCrawl                 
# archives. Generally, you should invoke it with the    
# runjob.sh shell script in the same directory, which   
# will launch multiple processes operating on different 
# subsets of CommonCrawl in parallel. Before doing that 
# you should also run setup.py to download the list     
# of WET URLs and the desired whitelist                 
#                                                       
# usage: python cluster.py [nodeID] [numNodes] [threshes] [whitelists]
#                                                       
# nodeID: The index of the node represented by this     
#           specific instance of the script             
#                                                       
# numNodes: The total number of nodes in the cluster.   
#           Determines how many archives to process.    
#                                                       
# threshes: A list of all thresholds, space-separated, enclosed in quotes
#
# whitelists: A list of all topics, space-separated, enclosed in quotes
#                                                       
# You should also pre-download the url list and the     
# whitelist by running setup.py before running this.    
#                                                       
# As a side note, if you want to test on a smaller      
# portion of CommonCrawl, just specify a very high      
# value for numNodes. E.g. 'cluster.py 0 12000 7.0'     
# will make the thread for this script evaluate only    
# three WET archives, since commoncrawl contains a      
# little over 24,000.                                   
########################################################

import boto3 #For getting S3 data from Commoncrawl
import nltk #For tokenizer
import os, sys, re

BAD_URL = 'does not pass'
CLEANING_REGEX = re.compile(r'[^a-zA-Z0-9]')
URL_OFFSET = len('WARC-Target-URI: ')

#Load the pre-downloaded list of urls
def getURLs(s3, nodeID, numNodes):
    counter = 0
    chosenURLs = []
    with open('urls') as urls:
        for line in urls:
            #Take only the archive urls this node should process
            if counter % numNodes == nodeID:
                chosenURLs.append(line[:len(line) - 1])
            counter += 1

    return chosenURLs

#Load the pre-downloaded whitelist, and return
#as a dictionary of term->weight
def getWeights(topics):
    weights = {}
    for topic in topics:
        weights[topic] = {}
        with open('whitelist-' + topic + '.txt') as f:
            for line in f:
                #Terms and weights are comma-delimited
                key = line[:line.find(',')]
                value = float(line[line.find(',') + 1: len(line) - 1])
                weights[topic][key] = value

    return weights


#Primary filter function - modifies results dict if the scoring
#function is over a given threshold
def filterPage(page, whitelists, thresholds, results):
    if not ' the ' in page: #check for english
        return results
    
    url = extractURL(page) #save the url before we destroy th epage
    page = CLEANING_REGEX.sub(' ', page) #Get rid of non alpha-numerics
    tokens = nltk.WordPunctTokenizer().tokenize(page)
    clean = [token.lower() for token in tokens] #All comparison in lower case
    freq = nltk.probability.FreqDist(clean)

    #The score of a document is the following formula
    # ( TF(term) / len(Document) ) * weight(term) * 1000

    #if freq['porn'] > 0:
        #return BAD_URL

    #^^^^ It was a nice thought
    #But porn is very persistent

    for topic in whitelists:
        totalScore = 0.0
        for term, weight in whitelists[topic].items():
            TF = 0
            if term.find(' ') > -1: #Multi-word term
                TF = clean.count(term)
            else: #Single word term
                TF = freq[term]
       
            weightedTF = (TF * 1.0) / len(clean)
            totalScore += weightedTF * weight * 1000 #Scale for pretty numbers

        #Give back the URL if it's acceptable
        if totalScore >= thresholds[topic]:
            resultString = url + ',' + str(totalScore)
            results[topic].append(resultString)

    return results


#Get's the URL given a commoncrawl archive - it's always
#Specified in the WARC-Target-URI field of the header
def extractURL(page):
    start = page.find('WARC-Target-URI') + URL_OFFSET
    end = page.find('WARC-Date') - 2 #discard the newline
    return page[start:end]

#Downloads the WET archive from a given url and processes
#Each of it's pages in turn. It's important to note that
#This process saves the accepted URLs from each archive separately.
#These all get combined with some bash scripting in runjob.sh after
#this script completes.
def processWETArchive(s3, url, whitelists, worker_id, counter, thresholds):
    print(whitelists)
    filename = str(worker_id)

    #Download and unzip the archive.
    s3.download_file("commoncrawl", url, filename + '.gz')
    os.system('gzip -d ' + filename + '.gz')

    results = {}
    for topic in whitelists:
        results[topic] = []
    print(results)

    with open(filename) as wetFile:
        pages = wetFile.read().split('WARC/1.0')
    
        accepted = []
        count = 0
        for page in pages:
            if count % 1000 == 0:
                 print(count)
            count += 1

            results = filterPage(page, whitelists, thresholds, results)

        #Use our worker's id as a filename to avoid conflicts with other threads
        for topic in results:
            with open(topic + '-output-node' + filename + '-' + str(counter) + '.txt', 'w') as outFile:
                for url in results[topic]:
                    outFile.write(url + '\n')
    
    os.system('echo "(`date +\"%H:%M:%S\"`) Worker #' + str(worker_id) + ' finished archive ' + str(counter) + '" >> logfile.txt')
    os.system('rm ' + filename) #clean up the archive we downloaded

def main():

    s3 = boto3.client('s3')

    urls = getURLs(s3, int(sys.argv[1]), int(sys.argv[2]))
    topics = sys.argv[4].split(' ')
    weights = getWeights(topics)

    #match each threshold to its weights
    threshes = sys.argv[3].split(' ')
    if not (len(threshes) == len(topics)):
        print('Error: Unequal number of topics and thresholds!')
        print('Exiting...')
        exit()

    thresholds = dict(zip(topics, [float(x) for x in threshes]))
    
    for counter, url in enumerate(urls):
        print("PROCESSING ARCHIVE # " + str(counter))
        processWETArchive(s3, url, weights, int(sys.argv[1]), counter, thresholds)

if __name__ == '__main__':
    if len(sys.argv) < 5:
        print("Invalid arguments. You probably made a mistake in preparing runjob.sh")
        print("Consult the top comment in the source code for more information")
        sys.exit(1)
    else:
        main()
        sys.exit(0)
