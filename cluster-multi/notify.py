#USAGE: python notify.py [TOPIC] [NODE_NUMBER]

#Called by runjob.sh to alert when the currently running task has finished
#You can change where the notification gets sent by changing the .credentials
#File in this directory. Obviously, make sure it stays in the .gitignore file, so
#the username and password don't end up online

import sys
import smtplib
import random
uid = ''
pwd = ''
to = ''

#For variety
exclamations = ['Nifty!', 'Wowzers!', 'Hooray!', 'Neat-o!', 'Well, would you look at that!', 'Yipee!', 'Eugapae!', 'Gadzooks!', 'Now there\'s something you don\'t see every day!']

with open('.credentials') as f:
    uid = next(f)[:-1]
    pwd = next(f)[:-1]
    to = next(f)[:-1]

exclam = exclamations[random.randint(0, len(exclamations) - 1)]
msg = exclam + ' The ' + sys.argv[1] + ' crawl on box #' + sys.argv[2] + ' just finished!'
server = smtplib.SMTP('smtp.gmail.com:587')
server.starttls()
server.login(uid, pwd)
server.sendmail('DM', to, msg)
server.quit()
