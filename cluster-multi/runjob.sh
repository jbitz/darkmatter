#USAGE: ./runjob.sh "[TOPICS]" "[THRESHOLDS]" [NODE_NUMBER]

#This shell script starts three threads all processing
#Different archives in commoncrawl. It is the recommended
#way to start a crawl as part of a larger cluster. The first
#argument to the script is the nodeID, and the second is the 
#number of nodes. The last argument is the threshold for
#page acceptance. Consult cluster.py for more details


#Set this constant as desired to influence # pages crawled
numNodes=30

echo "Setting up..."
touch logfile.txt
echo "----------BEGIN LOG FOR EC2 BOX $3----------" > logfile.txt
echo "" >> logfile.txt
echo "Start time: `date`" >> logfile.txt
echo "Topics: $1" >> logfile.txt
echo "" >> logfile.txt

echo python setup.py "$1" >> logfile.txt
echo "" >> logfile.txt
python setup.py "$1"

n1=$(($3 * 3 - 3))
n2=$(($3 * 3 - 2))
n3=$(($3 * 3 - 1))


echo "Starting crawl threads..."
echo "Starting crawl threads..." >> logfile.txt
echo python cluster.py $n1 $numNodes "$2" "$1" >> logfile.txt
echo python cluster.py $n2 $numNodes "$2" "$1" >> logfile.txt
echo python cluster.py $n3 $numNodes "$2" "$1" >> logfile.txt
echo "" >> logfile.txt

python cluster.py $n1 $numNodes "$2" "$1" &
python cluster.py $n2 $numNodes "$2" "$1" &
python cluster.py $n3 $numNodes "$2" "$1" &
wait

#Gather and upload results
echo "" >> logfile.txt
echo "Crawl completed! Uploading results..." >> logfile.txt
python uploadResults.py "$1" $3 >> logfile.txt
echo "" >> logfile.txt

#clean up
#rm finalList*
#rm whitelist*
#rm urls

#text jared
echo "Results uploaded! Notifying Jared..." >> logfile.txt
echo "" >> logfile.txt
python notify.py "$1" $3

#Save the log file to S3
echo "Finish time: `date`" >> logfile.txt
aws s3 cp logfile.txt s3://darkmatter-training/logs/`date -I`/box$3.log
