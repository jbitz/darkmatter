#!/bin/bash
#USAGE: ./downloadResults.sh [category]

mkdir $1 #Make directory to store results

for i in {1..10}
do
   aws s3 cp s3://darkmatter-training/$1/results/$1$i list$i.txt #Naming scheme is darkmatter-training/CATEGORY/results/CATEGORY[1-10] (no extension)
done

echo Aggregating files...
cat `ls | grep list` | sed 's/,/%2C/g' > ./$1/total.txt #The sed is to parse commas into URL form, since Mturk uses CSVs

echo Obtaining random samples...
touch ./$1/randomSample.csv
echo "url" > ./$1/randomSample.csv
shuf -n 500 ./$1/total.txt >> ./$1/randomSample.csv

echo Uploading to S3...
aws s3 cp ./$1/randomSample.csv s3://darkmatter-training/$1/results/forAMT.csv
aws s3 cp ./$1/total.txt s3://darkmatter-training/$1/results/total.txt

echo Cleaning up...
rm list*

echo 

echo Crawl obtained `cat ./$1/total.txt | wc -l` URLs!
