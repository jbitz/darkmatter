#!/usr/bin/env python
#USAGE ./launcher.py [TOPICS] [THRESHOLDS] [NUMNODES]

import socket, sys, os

ips = []
PORT = 4242
BUFFER_SIZE = 1024

with open('slaves.txt') as ipFile:
    for line in ipFile:
        ips.append(line[:-1])

for i in range(0, int(sys.argv[3])):
    #connect and send off command
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect( (ips[i], PORT) )
    paramString = '"' + sys.argv[1] + '" "' + sys.argv[2] + '" ' + str(i + 1)
    s.send(paramString.encode('utf-8'))
    
    #Verify from the handshake
    data = s.recv(BUFFER_SIZE)
    print('Received: ' + data.decode() + ' from node ' + str(i + 1))
    s.close()
